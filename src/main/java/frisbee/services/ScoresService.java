package frisbee.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

import frisbee.models.Score;

/**
 * The images used in the application.
 */
public class ScoresService {
	/**
	 * Saved scores
	 */
	public static ArrayList<Score> SCORES = new ArrayList<>();

	/**
	 * Separator used to separate scores lines
	 */
	private static final String SCORE_LINE_SEPARATOR = ";"; //$NON-NLS-1$

	/**
	 * Separator used to separate scores columns
	 */
	private static final String SCORE_COLUMN_SEPARATOR = ":"; //$NON-NLS-1$

	/**
	 * Prevents instantiation
	 */
	private ScoresService() {

	}

	/**
	 * initialize stored scores
	 */
	public static void initialize() {
		String scoresString = LocalStorageService.load(LocalStorageService.KEY_SCORES);
		if (scoresString == null) {
			return;
		}

		StringTokenizer scoresTokenizer = new StringTokenizer(scoresString, SCORE_LINE_SEPARATOR);
		while (scoresTokenizer.hasMoreTokens()) {
			StringTokenizer valuesTokenizer = new StringTokenizer(scoresTokenizer.nextToken(), SCORE_COLUMN_SEPARATOR);

			while (valuesTokenizer.hasMoreTokens()) {
				String playerName = valuesTokenizer.hasMoreTokens() ? valuesTokenizer.nextToken() : ""; //$NON-NLS-1$
				String scoreStr = valuesTokenizer.hasMoreTokens() ? valuesTokenizer.nextToken() : "0"; //$NON-NLS-1$
				String difficultyStr = valuesTokenizer.hasMoreTokens() ? valuesTokenizer.nextToken() : "0"; //$NON-NLS-1$
				String missionName = valuesTokenizer.hasMoreTokens() ? valuesTokenizer.nextToken() : ""; //$NON-NLS-1$
				String timestampStr = valuesTokenizer.hasMoreTokens() ? valuesTokenizer.nextToken() : "0"; //$NON-NLS-1$

				int scoreInt = Integer.parseInt(scoreStr);
				int difficulty = Integer.parseInt(difficultyStr);
				long timestamp = Long.parseLong(timestampStr);
				Score score = new Score(playerName, scoreInt, difficulty, missionName, timestamp);
				SCORES.add(0, score);
			}
		}
	}

	/**
	 * @param ordered
	 *            choose if list is ordered or not
	 * @return ordered scores list
	 */
	public static ArrayList<Score> getScores(boolean ordered) {
		if (!ordered) {
			return SCORES;
		}

		ArrayList<Score> orderedList = SCORES;
		Collections.sort(orderedList);
		return orderedList;
	}

	/**
	 * Get last score added
	 *
	 * @return last score added
	 */
	public static Score getLastScoreAdded() {
		Score lastScore = null;

		for (Score score : SCORES) {
			if (lastScore == null || lastScore.timestamp < score.timestamp) {
				lastScore = score;
			}
		}

		return lastScore;
	}

	/**
	 * Get index of last score added
	 *
	 * @return index of last score added
	 */
	public static int getLastScoreIndex() {
		if (SCORES.size() == 0) {
			return -1;
		}

		Score lastScore = null;
		ArrayList<Score> scoresOrdered = getScores(true);

		for (Score score : scoresOrdered) {
			if (lastScore == null || lastScore.timestamp < score.timestamp) {
				lastScore = score;
			}
		}

		return scoresOrdered.indexOf(lastScore);
	}

	/**
	 * @param score
	 *            new score to add
	 */
	public static void addScore(Score score) {
		String scoresString = LocalStorageService.load(LocalStorageService.KEY_SCORES) == null ? "" //$NON-NLS-1$
				: LocalStorageService.load(LocalStorageService.KEY_SCORES);
		scoresString += score.playerName + SCORE_COLUMN_SEPARATOR + score.score + SCORE_COLUMN_SEPARATOR
				+ score.difficulty + SCORE_COLUMN_SEPARATOR + score.missionName + SCORE_COLUMN_SEPARATOR
				+ score.timestamp + SCORE_LINE_SEPARATOR;
		LocalStorageService.store(LocalStorageService.KEY_SCORES, scoresString);
		SCORES.add(0, score);
	}
}
