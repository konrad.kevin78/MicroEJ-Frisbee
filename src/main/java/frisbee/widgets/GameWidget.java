package frisbee.widgets;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import ej.animation.Animation;
import ej.animation.Animator;
import ej.components.dependencyinjection.ServiceLoaderFactory;
import ej.microui.display.Colors;
import ej.microui.display.Font;
import ej.microui.display.GraphicsContext;
import ej.microui.display.transform.ImageScale;
import ej.microui.event.Event;
import ej.microui.event.generator.Pointer;
import ej.microui.util.EventHandler;
import ej.style.Style;
import ej.style.container.Rectangle;
import ej.widget.StyledWidget;
import frisbee.MainActivity;
import frisbee.models.DisplayableText;
import frisbee.models.Frisbee;
import frisbee.models.Item;
import frisbee.models.Mission;
import frisbee.models.Player;
import frisbee.models.Score;
import frisbee.models.SecondaryTarget;
import frisbee.models.Target;
import frisbee.pages.ScoresPage;
import frisbee.services.ImagesService;
import frisbee.services.ScoresService;
import frisbee.services.SettingsService;

/**
 *
 */
public class GameWidget extends StyledWidget implements Animation, EventHandler {
	private final Player player;
	private final Mission mission;
	private final ArrayList<Target> targets;
	private final ArrayList<Item> items;
	private final ArrayList<DisplayableText> rewardTexts;
	private int timer;
	private final long startTimer;
	private final long endTimer;
	private long lastLaunch;
	private int lastX;
	private int lastY;
	private int pressedX;
	private int pressedY;
	private double currentSpeed;
	private boolean ended = false;

	/**
	 * @param player
	 *            player of the game
	 * @param mission
	 *            selected mission
	 */
	public GameWidget(Player player, Mission mission) {
		this.player = player;
		this.mission = mission;
		this.targets = new ArrayList<Target>();
		this.items = new ArrayList<Item>();
		this.rewardTexts = new ArrayList<DisplayableText>();
		this.timer = 0;
		this.startTimer = System.currentTimeMillis();
		this.endTimer = this.startTimer + SettingsService.getGameDuration();
		ServiceLoaderFactory.getServiceLoader().getService(Animator.class, Animator.class).startAnimation(this);
	}

	@Override
	public void renderContent(GraphicsContext g, Style style, Rectangle bounds) {
		g.setFont(Font.getFont(Font.LATIN, 22, Font.STYLE_BOLD));
		g.setColor(Colors.NAVY);
		g.drawImage(this.mission.imageBackground, bounds.getX(), bounds.getY(),
				GraphicsContext.TOP | GraphicsContext.LEFT);

		for (DisplayableText rewardText : this.rewardTexts) {
			g.drawChars(rewardText.text.toCharArray(), 0, rewardText.text.length(), rewardText.x, rewardText.y,
					GraphicsContext.VCENTER | GraphicsContext.HCENTER);
		}

		for (Item item : this.items) {
			g.drawImage(item.image, item.x, item.y, GraphicsContext.HCENTER | GraphicsContext.VCENTER);
		}

		for (Target target : this.targets) {
			ImageScale scaling = ImageScale.Singleton;
			scaling.setFactor(0.5f + (5f / target.z));
			scaling.draw(g, target.image, target.x, target.y, GraphicsContext.HCENTER | GraphicsContext.VCENTER);
		}

		for (Frisbee frisbee : this.player.frisbees) {
			ImageScale scaling = ImageScale.Singleton;
			scaling.setFactor((float) Math.pow(frisbee.duration / 60f, 2));
			scaling.draw(g, frisbee.image, frisbee.x, frisbee.y, GraphicsContext.HCENTER | GraphicsContext.VCENTER);
		}

		String scoreString = String.valueOf(this.player.score);
		String timerString = SettingsService.GAME_TIMER_TEXT + this.timer;

		g.drawChars(scoreString.toCharArray(), 0, scoreString.length(), bounds.getWidth() / 2, 5,
				GraphicsContext.TOP | GraphicsContext.HCENTER);
		g.drawChars(timerString.toCharArray(), 0, timerString.length(), 5, 5,
				GraphicsContext.TOP | GraphicsContext.LEFT);
		g.drawImage(ImagesService.EXIT, bounds.getWidth() - 5, 5, GraphicsContext.TOP | GraphicsContext.RIGHT);
	}

	@Override
	public Rectangle validateContent(Style style, Rectangle bounds) {
		Rectangle rect = new Rectangle(bounds);
		rect.setSize(bounds.getWidth(), bounds.getHeight());
		return rect;
	}

	@Override
	public boolean tick(long currentTimeMillis) {
		if (this.ended) {
			return true;
		}

		this.updateTimer();
		this.spawnNewFrisbee();
		this.spawnNewTarget();
		this.updateFrisbees();
		this.updateTargets();
		this.updateRewardTexts();
		repaint();
		return true;
	}

	private void updateTimer() {
		this.timer = (int) (this.endTimer - System.currentTimeMillis()) / 1000;

		if (this.timer == 0) {
			this.endGame();
		}
	}

	private void endGame() {
		this.ended = true;

		if (this.player.score > 0) {
			ScoresService.addScore(new Score(this.player.name, this.player.score, SettingsService.CURRENT_DIFFICULTY,
					this.mission.name, new Date().getTime()));
		}

		MainActivity.show(new ScoresPage(true, this.player.name), false);
	}

	@Override
	public boolean handleEvent(int event) {
		if (Event.getType(event) == Event.POINTER) {
			Pointer ptr = (Pointer) Event.getGenerator(event);
			int x = ptr.getX();
			int y = ptr.getY();
			int action = Pointer.getAction(event);

			switch (action) {
			case Pointer.RELEASED:
				this.releaseFrisbee(x, y);
				break;
			case Pointer.DRAGGED:
				this.dragFrisbee(x, y);
				break;
			case Pointer.PRESSED:
				this.pressFrisbee(x, y);
				this.pressExitButton(x, y);
				break;
			}
		}

		return super.handleEvent(event);
	}

	private void releaseFrisbee(int x, int y) {
		Frisbee pressedFrisbee = getPressedFrisbee();
		if (pressedFrisbee == null) {
			return;
		}

		double angle = Math.atan2((y - this.pressedY), (x - this.pressedX));

		pressedFrisbee.speed = Math.min(Math.abs(this.currentSpeed), 35) / 3;
		pressedFrisbee.angle = angle;

		// TODO : implement curvating
		// int multiplier = this.pressedX > x ? 1 : -1;
		// double liftingPower = Math.sqrt(Math.pow(this.pressedX - 250, 2) + Math.pow(this.pressedY - 250, 2)) *
		// multiplier;
		// pressedFrisbee.lifting = liftingPower / 500;
		pressedFrisbee.lifting = 0;
		pressedFrisbee.pressed = false;
		pressedFrisbee.launched = true;
		this.lastLaunch = System.currentTimeMillis();
		this.currentSpeed = 1;
	}

	private void dragFrisbee(int x, int y) {
		Frisbee pressedFrisbee = getPressedFrisbee();
		if (pressedFrisbee == null) {
			return;
		}

		double distance = Math.sqrt(Math.pow(x - this.lastX, 2) + Math.pow(y - this.lastY, 2));
		this.currentSpeed = distance / 2;
		this.lastX = pressedFrisbee.x;
		this.lastY = pressedFrisbee.y;
	}

	private void pressFrisbee(int x, int y) {
		for (Frisbee frisbee : this.player.frisbees) {
			frisbee.pressed = false;
			this.pressedX = 250;
			this.pressedY = 250;

			if (!frisbee.launched && (x >= frisbee.x - frisbee.image.getWidth() / 2)
					&& (x <= frisbee.x + frisbee.image.getWidth() / 2)
					&& (y >= frisbee.y - frisbee.image.getHeight() / 2)
					&& (y <= frisbee.y + frisbee.image.getHeight() / 2)) {
				frisbee.pressed = true;
				this.pressedX = x;
				this.pressedY = y;
			}
		}
	}

	private void pressExitButton(int x, int y) {
		if ((x >= 480 - 5 - ImagesService.EXIT.getWidth()) && (x <= 480 - 5) && (y >= 5)
				&& (y <= 5 + ImagesService.EXIT.getHeight())) {
			this.endGame();
		}
	}

	private Frisbee getPressedFrisbee() {
		Frisbee pressedFrisbee = null;
		for (Frisbee frisbee : this.player.frisbees) {
			if (frisbee.pressed) {
				pressedFrisbee = frisbee;
			}
		}

		return pressedFrisbee;
	}

	private void spawnNewFrisbee() {
		if (System.currentTimeMillis() < this.lastLaunch + SettingsService.getFrisbeeDelay()) {
			return;
		}

		int count = 0;
		for (Frisbee frisbee : this.player.frisbees) {
			if (frisbee.launched) {
				count++;
			}
		}

		if (count == this.player.frisbees.size()) {
			this.player.frisbees.add(new Frisbee());
		}
	}

	private void spawnNewTarget() {
		if (this.targets.size() >= SettingsService.getMaxTargetsOnScreen()) {
			return;
		}

		int randomSpawn = new Random().nextInt(100);

		if (randomSpawn < SettingsService.TARGET_SEAGULL_SPAWN_RATE * SettingsService.getSpawnRateModifier()) {
			this.targets.add(
					new SecondaryTarget(this.mission.secondaryTargetImage, this.mission.secondaryTargetImageFlipped));
		} else if (randomSpawn < SettingsService.TARGET_SPAWN_RATE * SettingsService.getSpawnRateModifier()
				+ SettingsService.TARGET_SEAGULL_SPAWN_RATE * SettingsService.getSpawnRateModifier()) {
			this.targets.add(new Target(this.mission.mainTargetImage));
		}
	}

	private void updateFrisbees() {
		for (int i = 0; i < this.player.frisbees.size(); i++) {
			Frisbee frisbee = this.player.frisbees.get(i);

			if (frisbee.launched) {
				frisbee.duration--;
				frisbee.angle += frisbee.lifting;
				frisbee.x += frisbee.speed * Math.cos(frisbee.angle);
				frisbee.y += frisbee.speed * Math.sin(frisbee.angle);

				if (frisbee.duration == 0) {
					this.player.frisbees.remove(frisbee);
				}
			}

			for (int j = 0; j < this.targets.size(); j++) {
				Target target = this.targets.get(j);

				if (frisbee.launched && (target.x >= frisbee.x - frisbee.image.getWidth() / 2)
						&& (target.x <= frisbee.x + frisbee.image.getWidth() / 2)
						&& (target.y >= frisbee.y - frisbee.image.getHeight() / 2)
						&& (target.y <= frisbee.y + frisbee.image.getHeight() / 2)) {
					this.rewardTexts.add(new DisplayableText(target.x, target.y, "+" + String.valueOf(target.reward))); //$NON-NLS-1$
					this.targets.remove(target);
					this.player.score += target.reward;
					this.player.frisbees.remove(frisbee);
				}
			}
		}
	}

	private void updateTargets() {
		for (int i = 0; i < this.targets.size(); i++) {
			Target target = this.targets.get(i);
			target.duration--;

			if (target.duration == 0) {
				this.targets.remove(target);
				break;
			}

			if (target.speed > 0) {
				if (target.movementAxis == SettingsService.MOVING_AXIS_X) {
					target.x += target.speed * target.movementDirection;
				} else if (target.movementAxis == SettingsService.MOVING_AXIS_Y) {
					target.y += target.speed * target.movementDirection;
				}
			}
		}
	}

	private void updateRewardTexts() {
		for (int i = 0; i < this.rewardTexts.size(); i++) {
			DisplayableText text = this.rewardTexts.get(i);
			text.duration--;
			text.y += text.speed * text.movementDirection;

			if (text.duration <= 0) {
				this.rewardTexts.remove(text);
				break;
			}
		}
	}
}
