package frisbee.models;

import ej.microui.display.Image;

/**
 * Bonus item model
 */
public class Item {
	/**
	 * X position
	 */
	public int x;

	/**
	 * Y position
	 */
	public int y;

	/**
	 * Z position (not used)
	 */
	public int z;

	/**
	 * Image of the frisbee
	 */
	public Image image;
}
