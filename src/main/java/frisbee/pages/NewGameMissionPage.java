package frisbee.pages;

import ej.widget.basic.Button;
import ej.widget.basic.ButtonImage;
import ej.widget.basic.Label;
import ej.widget.container.List;
import ej.widget.container.Split;
import ej.widget.listener.OnClickListener;
import ej.widget.navigation.page.Page;
import frisbee.MainActivity;
import frisbee.models.Mission;
import frisbee.models.Player;
import frisbee.style.ClassSelectors;
import frisbee.style.Pictos;

/**
 * Settings page
 */
public class NewGameMissionPage extends Page {
	private static final String PAGE_TITLE = "Select mission"; //$NON-NLS-1$

	/**
	 * Constructor
	 *
	 * @param player
	 *            new player
	 */
	public NewGameMissionPage(final Player player) {
		super();

		final Mission missionBeach = new Mission("Beach"); //$NON-NLS-1$
		final Mission missionDesert = new Mission("Desert"); //$NON-NLS-1$

		// Top bar
		Split topBar = new Split(true, 0.1f);
		Label titleLabel = new Label(PAGE_TITLE);
		titleLabel.addClassSelector(ClassSelectors.TITLE);

		Button backButton = new Button(Character.toString(Pictos.BACK));
		backButton.addClassSelector(ClassSelectors.LARGE_ICON);
		backButton.addOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				MainActivity.show(new NewGamePlayerPage(player.name), false);
			}
		});

		topBar.setFirst(backButton);
		topBar.setLast(titleLabel);

		ButtonImage missionBeachBtn = new ButtonImage();
		missionBeachBtn.setSource(missionBeach.imageSelection);
		missionBeachBtn.addOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				MainActivity.show(new GamePage(player, missionBeach), true);
			}
		});

		ButtonImage missionDesertBtn = new ButtonImage();
		missionDesertBtn.setSource(missionDesert.imageSelection);
		missionDesertBtn.addOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				MainActivity.show(new GamePage(player, missionDesert), true);
			}
		});

		Split missionsSplit = new Split();
		missionsSplit.setFirst(missionBeachBtn);
		missionsSplit.setLast(missionDesertBtn);

		List optionsList = new List(false);
		optionsList.add(missionsSplit);

		// Page layout
		Split content = new Split(false, 0.3f);
		content.setFirst(topBar);
		content.setLast(optionsList);

		this.setWidget(content);
	}
}