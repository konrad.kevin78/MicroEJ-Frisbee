package frisbee.models;

import ej.microui.display.Image;
import frisbee.services.ImagesService;

/**
 * Player model
 */
public class Mission {
	/**
	 * Name of the player
	 */
	public final String name;

	/**
	 * Background image of the mission
	 */
	public Image imageBackground;

	/**
	 * Selection image of the mission
	 */
	public Image imageSelection;

	/**
	 * Image of the main target
	 */
	public Image mainTargetImage;

	/**
	 * Image of the secondary target
	 */
	public Image secondaryTargetImage;

	/**
	 * Image of the secondary target
	 */
	public Image secondaryTargetImageFlipped;

	/**
	 * @param name
	 *            name of the player
	 */
	public Mission(String name) {
		super();
		this.name = name;

		if (this.name.toLowerCase().equals("desert")) { //$NON-NLS-1$
			this.imageBackground = ImagesService.BACKGROUND_DESERT;
			this.imageSelection = ImagesService.MISSION_DESERT;
			this.mainTargetImage = ImagesService.TARGET_TUMBLEWEED;
			this.secondaryTargetImage = ImagesService.TARGET_TERRORIST;
			this.secondaryTargetImageFlipped = ImagesService.TARGET_TERRORIST_FLIP;

		} else if (this.name.toLowerCase().equals("beach")) { //$NON-NLS-1$
			this.imageBackground = ImagesService.BACKGROUND_BEACH;
			this.imageSelection = ImagesService.MISSION_BEACH;
			this.mainTargetImage = ImagesService.TARGET;
			this.secondaryTargetImage = ImagesService.TARGET_SEAGULL;
			this.secondaryTargetImageFlipped = ImagesService.TARGET_SEAGULL_FLIP;

		}

	}
}
