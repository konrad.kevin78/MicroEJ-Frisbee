package frisbee.pages;

import ej.components.dependencyinjection.ServiceLoaderFactory;
import ej.widget.basic.Button;
import ej.widget.basic.Label;
import ej.widget.composed.ButtonWrapper;
import ej.widget.container.List;
import ej.widget.container.Scroll;
import ej.widget.container.Split;
import ej.widget.listener.OnClickListener;
import ej.widget.navigation.page.Page;
import frisbee.MainActivity;
import frisbee.listeners.OnFocusListener;
import frisbee.models.Player;
import frisbee.services.SettingsService;
import frisbee.style.ClassSelectors;
import frisbee.style.Pictos;
import frisbee.widgets.KeyboardText;
import frisbee.widgets.Layout;
import frisbee.widgets.keyboard.Keyboard;
import frisbee.widgets.keyboard.LowerCaseLayout;
import frisbee.widgets.keyboard.NumericLayout;
import frisbee.widgets.keyboard.SymbolLayout;
import frisbee.widgets.keyboard.UpperCaseLayout;

/**
 * Settings page
 */
public class NewGamePlayerPage extends Page {
	private static final String PAGE_TITLE = "New game"; //$NON-NLS-1$
	private static final String NEW_GAME_PLAYER_NAME = "Player name : "; //$NON-NLS-1$
	private static final String START_GAME = "CONTINUE"; //$NON-NLS-1$
	private final Keyboard keyboard;
	private final KeyboardText playerName;
	private final Split content;
	private final Scroll optionsScroll;
	private final List optionsList;

	/**
	 * Constructor
	 *
	 * @param playerName
	 *            Last player name entered
	 */
	public NewGamePlayerPage(String playerName) {
		super();
		this.keyboard = new Keyboard();

		Layout[] layouts = new Layout[] { new LowerCaseLayout(), new UpperCaseLayout(), new NumericLayout(),
				new SymbolLayout() };
		setKeyboardLayouts(layouts);

		// Top bar
		Split topBar = new Split(true, 0.1f);
		Label titleLabel = new Label(PAGE_TITLE);
		titleLabel.addClassSelector(ClassSelectors.TITLE);

		Button backButton = new Button(Character.toString(Pictos.BACK));
		backButton.addClassSelector(ClassSelectors.LARGE_ICON);
		backButton.addOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				MainActivity.home();
			}
		});

		topBar.setFirst(backButton);
		topBar.setLast(titleLabel);

		// Options list
		Split playerNameSplit = new Split();
		Label playerNameLabel = new Label(NEW_GAME_PLAYER_NAME);
		String defaultName = playerName == null ? "" : playerName; //$NON-NLS-1$

		this.playerName = new KeyboardText(defaultName, SettingsService.PLAYER_NAME);
		this.playerName.setMaxTextLength(20);
		this.playerName.addOnFocusListener(new OnFocusListener() {
			@Override
			public void onGainFocus() {
				showKeyboard(true);
			}

			@Override
			public void onLostFocus() {
				// hideKeyboard();
			}
		});
		playerNameSplit.setFirst(playerNameLabel);
		playerNameSplit.setLast(this.playerName);

		ButtonWrapper startButton = new ButtonWrapper();
		Label startLabel = new Label(START_GAME);
		startButton.addClassSelector(ClassSelectors.START_BUTTON);
		startLabel.addClassSelector(ClassSelectors.START_LABEL);
		startButton.setWidget(startLabel);
		startButton.addOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				String name = NewGamePlayerPage.this.playerName.getText().isEmpty()
						? SettingsService.DEFAULT_PLAYER_NAME : NewGamePlayerPage.this.playerName.getText();
				Player player = new Player(name);
				MainActivity.show(new NewGameMissionPage(player), true);
			}
		});

		this.optionsList = new List(false);
		this.optionsList.add(playerNameSplit);
		this.optionsList.add(startButton);

		// Options scroll
		this.optionsScroll = new Scroll(false, false);
		this.optionsScroll.setWidget(this.optionsList);

		// Page layout
		this.content = new Split(false, 0.3f);
		this.content.setFirst(topBar);
		this.content.setLast(this.optionsScroll);

		this.setWidget(this.content);
	}

	/**
	 * Sets the keyboard layouts to use
	 *
	 * @param keyboardLayouts
	 *            the four keyboard layouts to use
	 */
	public void setKeyboardLayouts(Layout[] keyboardLayouts) {
		this.keyboard.setLayouts(keyboardLayouts);
	}

	/**
	 * Shows the keyboard
	 */
	protected void showKeyboard() {
		this.optionsList.remove(this.keyboard);
		if (this.keyboard.getParent() != this) {
			this.optionsList.add(this.keyboard);
		}

		revalidate();
	}

	/**
	 * Hides the keyboard
	 */
	protected void hideKeyboard() {
		this.optionsScroll.scrollTo(0);
		// this.optionsList.remove(this.keyboard);
		remove(this.keyboard);
		revalidate();
	}

	private void showKeyboard(boolean first) {
		showKeyboard();
		this.playerName.setActive(true);

		this.keyboard.setSpecialKey("OK", new OnClickListener() { //$NON-NLS-1$
			@Override
			public void onClick() {
				hideKeyboard();
			}
		});

		ej.microui.event.generator.Keyboard keyboard = ServiceLoaderFactory.getServiceLoader()
				.getService(ej.microui.event.generator.Keyboard.class);
		if (keyboard != null) {
			keyboard.setEventHandler(this.playerName);
		}
	}
}