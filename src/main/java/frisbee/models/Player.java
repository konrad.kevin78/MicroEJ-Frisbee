package frisbee.models;

import java.util.ArrayList;

/**
 * Player model
 */
public class Player {
	/**
	 * Name of the player
	 */
	public final String name;

	/**
	 * Current score of the player
	 */
	public int score;

	/**
	 * Remaining health of the player (not used)
	 */
	public int health;

	/**
	 * Frisbee launched and ready to be launched of the player
	 */
	public ArrayList<Frisbee> frisbees;

	/**
	 * @param name
	 *            name of the player
	 */
	public Player(String name) {
		super();
		this.name = name;
		this.score = 0;
		this.health = 100;
		this.frisbees = new ArrayList<>();
		this.frisbees.add(new Frisbee());
	}
}
