package frisbee.style;

/**
 * The font family used in the stylesheet.
 */
public interface FontFamilies {

	/**
	 * The picto font family.
	 */
	String PICTO = "picto"; //$NON-NLS-1$

	/**
	 * The roboto font family.
	 */
	String ROBOTO = "roboto"; //$NON-NLS-1$
}
