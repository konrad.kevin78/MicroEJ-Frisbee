package frisbee.services;

import ej.microui.display.Colors;
import ej.microui.display.Font;
import ej.microui.display.GraphicsContext;
import ej.style.Selector;
import ej.style.State;
import ej.style.Stylesheet;
import ej.style.background.NoBackground;
import ej.style.background.PlainBackground;
import ej.style.background.SimpleRoundedPlainBackground;
import ej.style.border.ComplexRectangularBorder;
import ej.style.border.SimpleRoundedBorder;
import ej.style.font.FontProfile;
import ej.style.font.FontProfile.FontSize;
import ej.style.outline.ComplexOutline;
import ej.style.outline.SimpleOutline;
import ej.style.selector.ClassSelector;
import ej.style.selector.EvenChildSelector;
import ej.style.selector.StateSelector;
import ej.style.selector.TypeSelector;
import ej.style.selector.combinator.AndCombinator;
import ej.style.selector.combinator.ChildCombinator;
import ej.style.text.ComplexTextManager;
import ej.style.util.EditableStyle;
import ej.style.util.StyleHelper;
import ej.widget.basic.ButtonImage;
import ej.widget.basic.Image;
import ej.widget.basic.Label;
import ej.widget.basic.drawing.CheckBox;
import ej.widget.basic.drawing.CircularProgressBar;
import ej.widget.basic.drawing.ProgressBar;
import ej.widget.basic.drawing.RadioBox;
import ej.widget.basic.drawing.Slider;
import ej.widget.basic.drawing.SwitchBox;
import ej.widget.basic.image.ImageCheck;
import ej.widget.basic.image.ImageRadio;
import ej.widget.basic.image.ImageSlider;
import ej.widget.basic.image.ImageSwitch;
import ej.widget.basic.picto.PictoCheck;
import ej.widget.basic.picto.PictoProgress;
import ej.widget.basic.picto.PictoRadio;
import ej.widget.basic.picto.PictoSlider;
import ej.widget.basic.picto.PictoSwitch;
import frisbee.style.ClassSelectors;
import frisbee.style.FontFamilies;
import frisbee.widgets.keyboard.Key;
import frisbee.widgets.keyboard.Keyboard;

/**
 * Class responsible for initializing the demo styles.
 */
public class StylesheetService {
	private static final int KEYBOARD_BACKGROUND_COLOR = Colors.WHITE;
	private static final int KEYBOARD_KEY_COLOR = Colors.BLACK;
	private static final int KEYBOARD_HIGHLIGHT_COLOR = Colors.GRAY;
	private static final int KEY_CORNER_RADIUS = 10;

	// Prevents initialization.
	private StylesheetService() {
	}

	/**
	 * Populates the stylesheet.
	 */
	public static void initialize() {
		Stylesheet stylesheet = StyleHelper.getStylesheet();

		// Sets the default style.
		EditableStyle defaultStyle = new EditableStyle();
		defaultStyle.setForegroundColor(Colors.WHITE);
		defaultStyle.setBackgroundColor(0x404041);
		FontProfile defaultFontProfile = new FontProfile();
		defaultFontProfile.setFamily(FontFamilies.ROBOTO);
		defaultFontProfile.setSize(FontSize.MEDIUM);
		defaultStyle.setFontProfile(defaultFontProfile);
		// defaultStyle.setBackground(NoBackground.NO_BACKGROUND);
		defaultStyle.setAlignment(GraphicsContext.LEFT | GraphicsContext.VCENTER);
		stylesheet.setDefaultStyle(defaultStyle);

		// Default margin not added in the default style because it also applies
		// for the composites.
		SimpleOutline defaultMargin = new SimpleOutline(6);

		TypeSelector labelTypeSelector = new TypeSelector(Label.class);
		TypeSelector checkboxTypeSelector = new TypeSelector(CheckBox.class);
		TypeSelector radioboxTypeSelector = new TypeSelector(RadioBox.class);
		TypeSelector switchboxTypeSelector = new TypeSelector(SwitchBox.class);
		TypeSelector pictocheckTypeSelector = new TypeSelector(PictoCheck.class);
		TypeSelector pictoradioTypeSelector = new TypeSelector(PictoRadio.class);
		TypeSelector pictoswitchTypeSelector = new TypeSelector(PictoSwitch.class);
		TypeSelector missionButtonTypeSelector = new TypeSelector(ButtonImage.class);
		StateSelector stateCheckedSelector = new StateSelector(State.Checked);
		ClassSelector listItemSelector = new ClassSelector(ClassSelectors.LIST_ITEM);
		ClassSelector listItemLastScoreSelector = new ClassSelector(ClassSelectors.LIST_ITEM_LAST_SCORE);
		ClassSelector listItem1stScoreSelector = new ClassSelector(ClassSelectors.LIST_ITEM_1ST_SCORE);
		ClassSelector listItem2ndScoreSelector = new ClassSelector(ClassSelectors.LIST_ITEM_2ND_SCORE);
		ClassSelector listItem3rdScoreSelector = new ClassSelector(ClassSelectors.LIST_ITEM_3RD_SCORE);

		EditableStyle btnStartStyle = new EditableStyle();
		SimpleOutline btnMargin = new SimpleOutline(7);
		btnStartStyle.setMargin(btnMargin);
		btnStartStyle.setPadding(btnMargin);
		btnStartStyle.setBackground(new SimpleRoundedPlainBackground(8));
		btnStartStyle.setBackgroundColor(Colors.RED);
		btnStartStyle.setForegroundColor(Colors.WHITE);
		btnStartStyle.setAlignment(GraphicsContext.HCENTER | GraphicsContext.VCENTER);
		ClassSelector btnStartSelector = new ClassSelector(ClassSelectors.START_BUTTON);
		ClassSelector labelStartSelector = new ClassSelector(ClassSelectors.START_LABEL);
		ChildCombinator parentStart = new ChildCombinator(btnStartSelector, labelStartSelector);
		stylesheet.addRule(parentStart, btnStartStyle);

		// Sets the label style.
		EditableStyle labelStyle = new EditableStyle();
		labelStyle.setPadding(defaultMargin);
		stylesheet.addRule(new AndCombinator(labelTypeSelector, listItemSelector), labelStyle);
		labelStyle.setBackground(NoBackground.NO_BACKGROUND);
		stylesheet.addRule(labelTypeSelector, labelStyle);

		EditableStyle selectionStyle = new EditableStyle();
		selectionStyle.setForegroundColor(Colors.GRAY);
		stylesheet.addRule(new ClassSelector(ClassSelectors.CLASS_SELECTOR_SELECTION), selectionStyle);

		EditableStyle missionWidgetStyle = new EditableStyle();
		missionWidgetStyle.setPadding(new SimpleOutline(15));
		stylesheet.addRule(missionButtonTypeSelector, missionWidgetStyle);

		EditableStyle keyboardStyle = new EditableStyle();
		keyboardStyle.setBackground(new PlainBackground());
		keyboardStyle.setBackgroundColor(KEYBOARD_BACKGROUND_COLOR);
		TypeSelector keyboardSelector = new TypeSelector(Keyboard.class);
		stylesheet.addRule(keyboardSelector, keyboardStyle);

		EditableStyle keyStyle = new EditableStyle();
		keyStyle.setForegroundColor(KEYBOARD_KEY_COLOR);
		keyStyle.setBackground(NoBackground.NO_BACKGROUND);
		keyStyle.setAlignment(GraphicsContext.HCENTER | GraphicsContext.VCENTER);
		keyStyle.setMargin(new ComplexOutline(4, 2, 4, 2));
		Selector keySelector = new TypeSelector(Key.class);
		stylesheet.addRule(keySelector, keyStyle);

		EditableStyle activeKeyStyle = new EditableStyle();
		activeKeyStyle.setForegroundColor(Colors.WHITE);
		activeKeyStyle.setBackground(new SimpleRoundedPlainBackground(KEY_CORNER_RADIUS));
		activeKeyStyle.setBackgroundColor(KEYBOARD_HIGHLIGHT_COLOR);
		activeKeyStyle.setBorder(new SimpleRoundedBorder(KEY_CORNER_RADIUS - 1, 1));
		activeKeyStyle.setBorderColor(KEYBOARD_HIGHLIGHT_COLOR);
		StateSelector activeSelector = new StateSelector(State.Active);
		AndCombinator activeKeySelector = new AndCombinator(keySelector, activeSelector);
		stylesheet.addRule(activeKeySelector, activeKeyStyle);

		EditableStyle spaceKeyStyle = new EditableStyle();
		spaceKeyStyle.setBackground(new SimpleRoundedPlainBackground(KEY_CORNER_RADIUS));
		spaceKeyStyle.setBackgroundColor(KEYBOARD_KEY_COLOR);
		spaceKeyStyle.setBorder(new SimpleRoundedBorder(KEY_CORNER_RADIUS - 1, 1));
		spaceKeyStyle.setBorderColor(KEYBOARD_KEY_COLOR);
		ClassSelector spaceKeySelector = new ClassSelector(ClassSelectors.SPACE_KEY_SELECTOR);
		stylesheet.addRule(spaceKeySelector, spaceKeyStyle);

		EditableStyle activeShiftKeyStyle = new EditableStyle();
		activeShiftKeyStyle.setBackground(new SimpleRoundedPlainBackground(KEY_CORNER_RADIUS));
		activeShiftKeyStyle.setBackgroundColor(Colors.GRAY);
		activeShiftKeyStyle.setBorder(new SimpleRoundedBorder(KEY_CORNER_RADIUS - 1, 1));
		activeShiftKeyStyle.setBorderColor(Colors.WHITE);
		ClassSelector activeShiftKeySelector = new ClassSelector(ClassSelectors.SHIFT_KEY_ACTIVE_SELECTOR);
		stylesheet.addRule(activeShiftKeySelector, activeShiftKeyStyle);

		EditableStyle specialKeyStyle = new EditableStyle();
		specialKeyStyle.setForegroundColor(Colors.WHITE);
		specialKeyStyle.setBackgroundColor(Colors.CYAN);
		specialKeyStyle.setBackground(new SimpleRoundedPlainBackground(KEY_CORNER_RADIUS));
		specialKeyStyle.setBorderColor(Colors.CYAN);
		specialKeyStyle.setBorder(new SimpleRoundedBorder(KEY_CORNER_RADIUS - 1, 1));
		FontProfile specialKeyFont = new FontProfile(FontFamilies.ROBOTO, FontSize.MEDIUM, Font.STYLE_PLAIN);
		specialKeyStyle.setFontProfile(specialKeyFont);
		ClassSelector specialKeySelector = new ClassSelector(ClassSelectors.SPECIAL_KEY_SELECTOR);
		stylesheet.addRule(specialKeySelector, specialKeyStyle);

		EditableStyle activeSpecialKeyStyle = new EditableStyle();
		activeSpecialKeyStyle.setBackgroundColor(Colors.GRAY);
		activeSpecialKeyStyle.setBorderColor(Colors.GRAY);
		stylesheet.addRule(new AndCombinator(specialKeySelector, new StateSelector(State.Active)),
				activeSpecialKeyStyle);

		// Sets the large picto style.
		EditableStyle largePictoStyle = new EditableStyle();
		largePictoStyle.setFontProfile(new FontProfile(FontFamilies.PICTO, FontSize.LARGE, Font.STYLE_PLAIN));
		stylesheet.addRule(new ClassSelector(ClassSelectors.LARGE_ICON), largePictoStyle);

		EditableStyle clearButtonStyle = new EditableStyle();
		clearButtonStyle.setAlignment(GraphicsContext.RIGHT | GraphicsContext.VCENTER);
		clearButtonStyle.setForegroundColor(Colors.BLACK);
		FontProfile clearButtonFont = new FontProfile(FontFamilies.ROBOTO, FontSize.LARGE, Font.STYLE_PLAIN);
		clearButtonStyle.setFontProfile(clearButtonFont);
		stylesheet.addRule(new ClassSelector(ClassSelectors.CLASS_SELECTOR_CLEAR_BUTTON), clearButtonStyle);

		// Sets the title style.
		EditableStyle titleStyle = new EditableStyle();
		titleStyle.setFontProfile(new FontProfile(FontFamilies.ROBOTO, FontSize.LARGE, Font.STYLE_PLAIN));
		ComplexRectangularBorder titleBorder = new ComplexRectangularBorder();
		titleBorder.setBottom(2);
		titleStyle.setBackgroundColor(Colors.SILVER);
		titleStyle.setBorder(titleBorder);
		stylesheet.addRule(new ClassSelector(ClassSelectors.TITLE), titleStyle);

		// Sets the list item style.
		EditableStyle listItemStyle = new EditableStyle();
		// ComplexRectangularBorder listItemBorder = new
		// ComplexRectangularBorder();
		// listItemBorder.setBottom(1);
		// listItemBorder.setColorBottom(Colors.GRAY);
		// listItemStyle.setBorder(listItemBorder);
		listItemStyle.setMargin(new ComplexOutline(0, 4, 0, 4));
		stylesheet.addRule(listItemSelector, listItemStyle);

		// Sets the list item style.
		EditableStyle listItemLastScoreStyle = new EditableStyle();
		listItemLastScoreStyle.setBackgroundColor(Colors.GREEN);
		listItemLastScoreStyle.setMargin(new ComplexOutline(0, 4, 0, 4));
		stylesheet.addRule(listItemLastScoreSelector, listItemLastScoreStyle);

		// 1st score
		EditableStyle listItem1stScoreStyle = new EditableStyle();
		listItem1stScoreStyle.setBackgroundColor(0xffc700);
		listItem1stScoreStyle.setMargin(new ComplexOutline(0, 4, 0, 4));
		stylesheet.addRule(listItem1stScoreSelector, listItem1stScoreStyle);

		// 2nd score
		EditableStyle listItem2ndScoreStyle = new EditableStyle();
		listItem2ndScoreStyle.setBackgroundColor(0xffa500);
		listItem2ndScoreStyle.setMargin(new ComplexOutline(0, 4, 0, 4));
		stylesheet.addRule(listItem2ndScoreSelector, listItem2ndScoreStyle);

		// 3rd score
		EditableStyle listItem3rdScoreStyle = new EditableStyle();
		listItem3rdScoreStyle.setBackgroundColor(0xff4500);
		listItem3rdScoreStyle.setMargin(new ComplexOutline(0, 4, 0, 4));
		stylesheet.addRule(listItem3rdScoreSelector, listItem3rdScoreStyle);

		EditableStyle evenListItemStyle = new EditableStyle();
		evenListItemStyle.setBackground(new PlainBackground());
		evenListItemStyle.setBackgroundColor(0x505051);
		stylesheet.addRule(new AndCombinator(listItemSelector, new EvenChildSelector()), evenListItemStyle);

		// Sets the image style.
		EditableStyle imageStyle = new EditableStyle();
		// Align with back button size.
		imageStyle.setPadding(new ComplexOutline(0, 0, 0, 5));
		stylesheet.addRule(new TypeSelector(Image.class), imageStyle);

		// Sets the unchecked toggle style.
		EditableStyle toggleStyle = new EditableStyle();
		toggleStyle.setForegroundColor(0xbcbec0);
		toggleStyle.setMargin(defaultMargin);
		stylesheet.addRule(checkboxTypeSelector, toggleStyle);
		stylesheet.addRule(radioboxTypeSelector, toggleStyle);
		stylesheet.addRule(switchboxTypeSelector, toggleStyle);

		// The font to use for the most of the picto widgets.
		FontProfile widgetPictoFontProfile = new FontProfile(FontFamilies.PICTO, FontSize.MEDIUM, Font.STYLE_PLAIN);

		// Sets the unchecked picto toggle style.
		EditableStyle pictoToggleStyle = new EditableStyle();
		pictoToggleStyle.setFontProfile(widgetPictoFontProfile);
		pictoToggleStyle.setForegroundColor(0xbcbec0);
		pictoToggleStyle.setMargin(defaultMargin);
		stylesheet.addRule(pictocheckTypeSelector, pictoToggleStyle);
		stylesheet.addRule(pictoradioTypeSelector, pictoToggleStyle);
		stylesheet.addRule(pictoswitchTypeSelector, pictoToggleStyle);

		// Sets the widget and checked toggle style.
		EditableStyle widgetStyle = new EditableStyle();
		widgetStyle.setMargin(defaultMargin);
		widgetStyle.setForegroundColor(0x10bdf1);
		stylesheet.addRule(new TypeSelector(ProgressBar.class), widgetStyle);
		stylesheet.addRule(new TypeSelector(CircularProgressBar.class), widgetStyle);
		stylesheet.addRule(new TypeSelector(Slider.class), widgetStyle);
		stylesheet.addRule(new AndCombinator(checkboxTypeSelector, stateCheckedSelector), widgetStyle);
		stylesheet.addRule(new AndCombinator(radioboxTypeSelector, stateCheckedSelector), widgetStyle);
		stylesheet.addRule(new AndCombinator(switchboxTypeSelector, stateCheckedSelector), widgetStyle);

		// Sets the image widget style.
		EditableStyle widgetImageStyle = new EditableStyle();
		widgetImageStyle.setMargin(defaultMargin);
		stylesheet.addRule(new TypeSelector(ImageSlider.class), widgetImageStyle);
		stylesheet.addRule(new TypeSelector(ImageRadio.class), widgetImageStyle);
		stylesheet.addRule(new TypeSelector(ImageCheck.class), widgetImageStyle);
		stylesheet.addRule(new TypeSelector(ImageSwitch.class), widgetImageStyle);

		// Sets the picto widget and checked picto toggle style.
		EditableStyle widgetPictoStyle = new EditableStyle();
		widgetPictoStyle.setMargin(defaultMargin);
		widgetPictoStyle.setForegroundColor(0x10bdf1);
		widgetPictoStyle.setFontProfile(widgetPictoFontProfile);
		stylesheet.addRule(new TypeSelector(PictoSlider.class), widgetPictoStyle);
		stylesheet.addRule(new AndCombinator(pictocheckTypeSelector, stateCheckedSelector), widgetPictoStyle);
		stylesheet.addRule(new AndCombinator(pictoradioTypeSelector, stateCheckedSelector), widgetPictoStyle);
		stylesheet.addRule(new AndCombinator(pictoswitchTypeSelector, stateCheckedSelector), widgetPictoStyle);
		stylesheet.addRule(new TypeSelector(PictoProgress.class), widgetPictoStyle);

		// Sets the illustrated button style.
		ClassSelector illustratedButtonSelector = new ClassSelector(ClassSelectors.ILLUSTRATED_BUTTON);
		EditableStyle buttonStyle = new EditableStyle();
		buttonStyle.setBackgroundColor(0x10bdf1);
		buttonStyle.setMargin(new ComplexOutline(12, 60, 12, 60));
		// The content of the button is centered horizontally and vertically.
		buttonStyle.setAlignment(GraphicsContext.HCENTER | GraphicsContext.VCENTER);
		stylesheet.addRule(illustratedButtonSelector, buttonStyle);

		// Sets the illustrated active button style.
		EditableStyle activeButtonStyle = new EditableStyle();
		activeButtonStyle.setBackgroundColor(0x1185a8);
		stylesheet.addRule(new AndCombinator(illustratedButtonSelector, new StateSelector(State.Active)),
				activeButtonStyle);

		// Sets the text title style.
		EditableStyle textTitleStyle = new EditableStyle();
		ComplexRectangularBorder textTitleBorder = new ComplexRectangularBorder();
		textTitleBorder.setBottom(1);
		textTitleStyle.setBackgroundColor(Colors.SILVER);
		textTitleStyle.setBorder(textTitleBorder);
		stylesheet.addRule(new ClassSelector(ClassSelectors.TEXT_TITLE), textTitleStyle);

		// Sets the multiline style.
		EditableStyle multilineStyle = new EditableStyle();
		ComplexTextManager complexTextManager = new ComplexTextManager(40);
		multilineStyle.setTextManager(complexTextManager);
		stylesheet.addRule(new ClassSelector(ClassSelectors.MULTILINE), multilineStyle);
	}
}
