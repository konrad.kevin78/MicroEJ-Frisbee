package frisbee.services;

/**
 * The images used in the application.
 */
public class SettingsService {
	/**
	 * Game easy difficulty setting
	 */
	public static int EASY_DIFFICULTY = 1;

	/**
	 * Game normal difficulty setting
	 */
	public static int NORMAL_DIFFICULTY = 2;

	/**
	 * Game hard difficulty setting
	 */
	public static int HARD_DIFFICULTY = 3;

	/**
	 * Game default difficulty setting
	 */
	public static int DEFAULT_DIFFICULTY = NORMAL_DIFFICULTY;

	/**
	 * Game current difficulty setting
	 */
	public static int CURRENT_DIFFICULTY = DEFAULT_DIFFICULTY;

	/**
	 * Player name placeholder
	 */
	public static String PLAYER_NAME = "Enter name"; //$NON-NLS-1$

	/**
	 * Player name placeholder
	 */
	public static String DEFAULT_PLAYER_NAME = "Player"; //$NON-NLS-1$

	/**
	 * Axis X string
	 */
	public static String MOVING_AXIS_X = "x"; //$NON-NLS-1$

	/**
	 * Axis Y strings
	 */
	public static String MOVING_AXIS_Y = "y"; //$NON-NLS-1$

	/**
	 * Base spawn rate of the targets
	 */
	public static final int TARGET_SPAWN_RATE = 4;

	/**
	 * Base spawn rate of the targets
	 */
	public static final int TARGET_SEAGULL_SPAWN_RATE = 1;

	/**
	 * Game bonus items enabled
	 */
	public static boolean BONUS_ITEMS_ENABLED = true;

	/**
	 * Game targets moving
	 */
	public static boolean TARGETS_MOVING_ENABLED = true;

	/**
	 * Game timer text
	 */
	public static String GAME_TIMER_TEXT = "Timer : "; //$NON-NLS-1$

	/**
	 * Game timer text
	 */
	public static long GAME_FRISBEE_DELAY_EASY = 750;

	/**
	 * Game timer text
	 */
	public static long GAME_FRISBEE_DELAY_NORMAL = 500;

	/**
	 * Game timer text
	 */
	public static long GAME_FRISBEE_DELAY_HARD = 250;

	/**
	 * Game timer text
	 */
	public static long GAME_DURATION_EASY = 100000;

	/**
	 * Game timer text
	 */
	public static long GAME_DURATION_NORMAL = 80000;

	/**
	 * Game timer text
	 */
	public static long GAME_DURATION_HARD = 60000;

	/**
	 * Prevents instantiation
	 */
	private SettingsService() {
	}

	/**
	 * @param difficulty
	 *            difficulty id
	 * @return difficult string
	 */
	public static String getDifficultyString(int difficulty) {
		switch (difficulty) {
		case 1:
			return "Easy"; //$NON-NLS-1$
		case 2:
			return "Normal"; //$NON-NLS-1$
		case 3:
			return "Hard"; //$NON-NLS-1$
		default:
			return "Normal"; //$NON-NLS-1$
		}
	}

	/**
	 * @return frisbee delay between 2 shots
	 */
	public static long getFrisbeeDelay() {
		switch (CURRENT_DIFFICULTY) {
		case 1:
			return GAME_FRISBEE_DELAY_EASY;
		case 2:
			return GAME_FRISBEE_DELAY_NORMAL;
		case 3:
			return GAME_FRISBEE_DELAY_HARD;
		default:
			return GAME_FRISBEE_DELAY_NORMAL;
		}
	}

	/**
	 * @return frisbee delay between 2 shots
	 */
	public static long getMaxTargetsOnScreen() {
		switch (CURRENT_DIFFICULTY) {
		case 1:
			return 5;
		case 2:
			return 15;
		case 3:
			return 30;
		default:
			return GAME_FRISBEE_DELAY_NORMAL;
		}
	}

	/**
	 * @return game duration
	 */
	public static long getGameDuration() {
		switch (CURRENT_DIFFICULTY) {
		case 1:
			return GAME_DURATION_EASY;
		case 2:
			return GAME_DURATION_NORMAL;
		case 3:
			return GAME_DURATION_HARD;
		default:
			return GAME_DURATION_NORMAL;
		}
	}

	/**
	 * @return game duration
	 */
	public static double getSpawnRateModifier() {
		switch (CURRENT_DIFFICULTY) {
		case 1:
			return 1;
		case 2:
			return 1.2;
		case 3:
			return 1.4;
		default:
			return 1.2;
		}
	}

	/**
	 *
	 */
	public static void initialize() {
		String storedDifficulty = LocalStorageService.load(LocalStorageService.KEY_SETTINGS_DIFFICULTY);
		String storedBonusItems = LocalStorageService.load(LocalStorageService.KEY_SETTINGS_BONUS_ITEMS_ENABLED);
		String storedMovingTargets = LocalStorageService.load(LocalStorageService.KEY_SETTINGS_MOVING_TARGET_ENABLED);

		if (storedDifficulty == null) {
			LocalStorageService.store(LocalStorageService.KEY_SETTINGS_DIFFICULTY, String.valueOf(DEFAULT_DIFFICULTY));
		}

		if (storedBonusItems == null) {
			LocalStorageService.store(LocalStorageService.KEY_SETTINGS_BONUS_ITEMS_ENABLED, String.valueOf(true));
		}

		if (storedMovingTargets == null) {
			LocalStorageService.store(LocalStorageService.KEY_SETTINGS_MOVING_TARGET_ENABLED, String.valueOf(true));
		}

		CURRENT_DIFFICULTY = storedDifficulty != null ? Integer.parseInt(storedDifficulty) : DEFAULT_DIFFICULTY;
		BONUS_ITEMS_ENABLED = storedBonusItems != null ? Boolean.parseBoolean(storedBonusItems) : true;
		TARGETS_MOVING_ENABLED = storedMovingTargets != null ? Boolean.parseBoolean(storedMovingTargets) : true;
	}
}
