package frisbee.models;

import ej.microui.display.Image;
import frisbee.services.SettingsService;

/**
 *
 */
public class SecondaryTarget extends Target {
	/**
	 * Constructor
	 *
	 * @param image
	 *            image of the secondary target
	 */
	public SecondaryTarget(Image image, Image imageFlip) {
		super(image);

		int randomY = (int) (200 - Math.pow(super.z / 5, 2) * 4);

		super.y = randomY;
		super.speed = 10;
		super.movementAxis = SettingsService.MOVING_AXIS_X;
		super.reward = 250;
		super.image = image;

		if (super.movementDirection == 1) {
			super.image = imageFlip;
		}
	}
}
